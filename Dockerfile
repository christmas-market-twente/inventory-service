#######################
## STEP 1: Build JAR ##
#######################
FROM openjdk:11-jdk AS builder

WORKDIR /app

# Install dependencies first
COPY .mvn .mvn
COPY pom.xml mvnw ./

RUN chmod +x mvnw
RUN ./mvnw dependency:go-offline

# Build application
COPY src src
RUN ./mvnw -Dmaven.test.skip=true package

###############################
## STEP 2: Build small image ##
###############################
FROM adoptopenjdk/openjdk11:alpine-jre

# Copy the JAR from the builder
COPY --from=builder /app/target/*.jar /app.jar

# Create a non-root user
RUN addgroup -S java && adduser -S javauser -G java
USER javauser

EXPOSE 8080

ENTRYPOINT ["java", "-jar", "/app.jar"]
