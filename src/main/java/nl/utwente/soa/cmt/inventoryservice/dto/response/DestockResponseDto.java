package nl.utwente.soa.cmt.inventoryservice.dto.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class DestockResponseDto {

    private Integer stock;
    private Integer price;

    public DestockResponseDto(Integer stock, Integer price) {
        this.stock = stock;
        this.price = price;
    }

    public DestockResponseDto() {}
}
