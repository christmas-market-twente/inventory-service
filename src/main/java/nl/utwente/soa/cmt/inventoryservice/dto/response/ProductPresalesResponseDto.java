package nl.utwente.soa.cmt.inventoryservice.dto.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import nl.utwente.soa.cmt.inventoryservice.model.Product;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductPresalesResponseDto {

    private Long id;

    private Integer amount;

    private Integer price;

    private String vendor;

    public ProductPresalesResponseDto(Long id, Integer amount, Integer price, String vendor) {
        this.id = id;
        this.amount = amount;
        this.price = price;
        this.vendor = vendor;
    }

    public ProductPresalesResponseDto() {
    }

    public static ProductPresalesResponseDto from(Product product) {
        return new ProductPresalesResponseDto(
                product.getId(),
                product.getAmount(),
                product.getPrice(),
                product.getVendor()
        );
    }
}
