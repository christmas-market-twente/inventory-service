package nl.utwente.soa.cmt.inventoryservice.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import nl.utwente.soa.cmt.inventoryservice.dto.request.MassRestockRequestDto;
import nl.utwente.soa.cmt.inventoryservice.dto.request.RestockRequestDto;
import nl.utwente.soa.cmt.inventoryservice.dto.response.DestockResponseDto;
import nl.utwente.soa.cmt.inventoryservice.dto.response.ProductStockResponseDto;
import nl.utwente.soa.cmt.inventoryservice.dto.response.RestockResponseDto;
import nl.utwente.soa.cmt.inventoryservice.exception.NotEnoughStockException;
import nl.utwente.soa.cmt.inventoryservice.exception.ProductNotFoundException;
import nl.utwente.soa.cmt.inventoryservice.response.ErrorResponse;
import nl.utwente.soa.cmt.inventoryservice.service.StockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/products")
@Tag(name = "Stock", description = "The Stock API")
public class StockController {

    @Autowired
    private StockService stockService;

    @Operation(summary = "Restock a product.", description = "Adds stock and returns the product.", tags = {"Stock"})
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "Successful operation"),
        @ApiResponse(responseCode = "400", description = "Invalid Request Body",
            content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
        @ApiResponse(responseCode = "404", description = "No product was found with the specified ID",
            content = @Content(schema = @Schema(implementation = ErrorResponse.class)))})
    @PutMapping("/{id}/stock/replenish")
    public RestockResponseDto restock(@Parameter(description = "The ID of the product to be updated.", required = true)
                                          @PathVariable Long id,
                                      @Parameter(description = "The amount of stock to be added.")
                                          @Valid @RequestBody RestockRequestDto restockRequestDto)
            throws ProductNotFoundException {
        return stockService.restock(id, restockRequestDto);
    }

    @Operation(summary = "Remove stock of a product.", description = "Removes stock and returns the product.", tags = {"Stock"})
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "Successful operation"),
        @ApiResponse(responseCode = "400", description = "Invalid Request Body",
            content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
        @ApiResponse(responseCode = "404", description = "No product was found with the specified ID",
            content = @Content(schema = @Schema(implementation = ErrorResponse.class)))})
    @PutMapping("/{id}/stock/remove")
    public DestockResponseDto destock(@Parameter(description = "The ID of the product to be updated.", required = true)
                                          @PathVariable Long id,
                                      @Parameter(description = "The amount of stock to be removed.")
                                          @Valid @RequestBody RestockRequestDto restockRequestDto)
            throws ProductNotFoundException, NotEnoughStockException {
        return stockService.destock(id, restockRequestDto);
    }

    @Operation(summary = "Remove stock of a number of product.", description = "Removes stock and returns a list of the products.", tags = {"Stock"})
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "Successful operation"),
        @ApiResponse(responseCode = "400", description = "Invalid Request Body",
            content = @Content(schema = @Schema(implementation = ErrorResponse.class)))})
    @PutMapping("/stock/remove")
    public List<ProductStockResponseDto> massDestock(@Parameter(description = "List of all products and amounts to remove stock of.")
                                                         @Valid @RequestBody MassRestockRequestDto massRestockRequestDto)
                                        throws ProductNotFoundException, NotEnoughStockException{
        return stockService.massDestock(massRestockRequestDto);
    }

    @Operation(summary = "Adds stock of a number of product.", description = "Adds stock and returns a list of the products.", tags = {"Stock"})
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "Successful operation"),
        @ApiResponse(responseCode = "400", description = "Invalid Request Body",
            content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
        @ApiResponse(responseCode = "404", description = "No product was found with the specified ID",
            content = @Content(schema = @Schema(implementation = ErrorResponse.class)))})
    @PutMapping("/stock/revert")
    public List<ProductStockResponseDto> massRestock(@Parameter(description = "List of all products and amounts to add stock of.")
                                                         @Valid @RequestBody MassRestockRequestDto massRestockRequestDto)
                                        throws ProductNotFoundException {
        return stockService.massRestock(massRestockRequestDto);
    }

}
