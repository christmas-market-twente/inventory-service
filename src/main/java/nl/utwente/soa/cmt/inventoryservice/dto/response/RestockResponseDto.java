package nl.utwente.soa.cmt.inventoryservice.dto.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class RestockResponseDto {

    private Integer stock;

    public RestockResponseDto(Integer stock) {
        this.stock = stock;
    }

    public RestockResponseDto() {}
}
