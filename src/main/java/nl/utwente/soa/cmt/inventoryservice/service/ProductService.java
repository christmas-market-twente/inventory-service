package nl.utwente.soa.cmt.inventoryservice.service;

import nl.utwente.soa.cmt.inventoryservice.dto.request.CreateProductRequestDto;
import nl.utwente.soa.cmt.inventoryservice.dto.request.UpdateProductRequestDto;
import nl.utwente.soa.cmt.inventoryservice.dto.response.ProductPresalesResponseDto;
import nl.utwente.soa.cmt.inventoryservice.dto.response.ProductResponseDto;
import nl.utwente.soa.cmt.inventoryservice.exception.ProductNotFoundException;
import nl.utwente.soa.cmt.inventoryservice.model.Product;
import nl.utwente.soa.cmt.inventoryservice.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProductService {

    @Autowired
    private ProductRepository productRepository;

    public List<ProductResponseDto> getAllProducts() {
        return productRepository.findAll().stream().map(ProductResponseDto::from).collect(Collectors.toList());
    }

    public ProductResponseDto getProduct(Long id) throws ProductNotFoundException {
        return productRepository.findById(id).map(ProductResponseDto::from).orElseThrow(ProductNotFoundException::new);
    }

    public List<ProductPresalesResponseDto> getProductPresale(Long[] ids) throws ProductNotFoundException {
        List<ProductPresalesResponseDto> productInfo = new ArrayList<>();
        for(Long id : ids) {
            productInfo.add(productRepository.findById(id).map(ProductPresalesResponseDto::from)
                    .orElseThrow(ProductNotFoundException::new));
        }
        return productInfo;
    }

    public ProductResponseDto createProduct(CreateProductRequestDto createProductRequestDto) {
        Product product = new Product();
        product.setName(createProductRequestDto.getName());
        product.setDescription(createProductRequestDto.getDescription());
        product.setAmount(createProductRequestDto.getAmount());
        product.setPrice(createProductRequestDto.getPrice());
        product.setVendor(createProductRequestDto.getVendor());
        product.setImageUrl(createProductRequestDto.getImageUrl());

        return ProductResponseDto.from(
                productRepository.save(product)
        );
    }

    public ProductResponseDto updateProduct(Long id, UpdateProductRequestDto updateProductRequestDto) throws ProductNotFoundException {
        Product product = productRepository.findById(id).orElseThrow(ProductNotFoundException::new);
        if (updateProductRequestDto.getName() != null) {
            product.setName(updateProductRequestDto.getName());
        }
        if (updateProductRequestDto.getDescription() != null) {
            product.setDescription(updateProductRequestDto.getDescription());
        }
        if (updateProductRequestDto.getPrice() != null) {
            product.setPrice(updateProductRequestDto.getPrice());
        }
        if (updateProductRequestDto.getImageUrl() != null) {
            product.setImageUrl(updateProductRequestDto.getImageUrl());
        }

        return ProductResponseDto.from(
                productRepository.save(product)
        );
    }

    public void deleteProduct(Long id) throws ProductNotFoundException {
        if (!productRepository.existsById(id)) throw new ProductNotFoundException();
        productRepository.deleteById(id);
    }
}
