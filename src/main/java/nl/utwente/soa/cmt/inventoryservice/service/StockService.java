package nl.utwente.soa.cmt.inventoryservice.service;

import nl.utwente.soa.cmt.inventoryservice.dto.request.MassRestockRequestDto;
import nl.utwente.soa.cmt.inventoryservice.dto.request.ProductRestockRequestDto;
import nl.utwente.soa.cmt.inventoryservice.dto.request.RestockRequestDto;
import nl.utwente.soa.cmt.inventoryservice.dto.response.DestockResponseDto;
import nl.utwente.soa.cmt.inventoryservice.dto.response.ProductStockResponseDto;
import nl.utwente.soa.cmt.inventoryservice.dto.response.RestockResponseDto;
import nl.utwente.soa.cmt.inventoryservice.exception.NotEnoughStockException;
import nl.utwente.soa.cmt.inventoryservice.exception.ProductNotFoundException;
import nl.utwente.soa.cmt.inventoryservice.model.Product;
import nl.utwente.soa.cmt.inventoryservice.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class StockService {

    @Autowired
    private ProductRepository productRepository;

    public Optional<Product> getProduct(Long id) {
        return productRepository.findById(id);
    }

    public RestockResponseDto restock(Long id, RestockRequestDto restockRequestDto) throws ProductNotFoundException {
        Product product = getProduct(id).orElseThrow(ProductNotFoundException::new);
        Integer newAmount = product.getAmount() + restockRequestDto.getChange();
        product.setAmount(newAmount);
        productRepository.save(product);

        return new RestockResponseDto(product.getAmount());
    }

    public DestockResponseDto destock(Long id, RestockRequestDto restockRequestDto)
                                    throws ProductNotFoundException, NotEnoughStockException {
        Product product = getProduct(id).orElseThrow(ProductNotFoundException::new);
        Integer newAmount = product.getAmount() - restockRequestDto.getChange();
        if (newAmount < 0) throw new NotEnoughStockException();
        product.setAmount(newAmount);
        productRepository.save(product);

        return new DestockResponseDto(product.getAmount(), product.getPrice());
    }

    public List<ProductStockResponseDto> massRestock(MassRestockRequestDto massRestockRequestDto)
            throws ProductNotFoundException {
        HashMap<Product, Integer> toRestock = new HashMap<>();
        for(ProductRestockRequestDto restockInfo : massRestockRequestDto.getProducts()) {
            Product product = getProduct(restockInfo.getId()).orElseThrow(ProductNotFoundException::new);
            Integer newAmount = product.getAmount() + restockInfo.getChange();
            toRestock.put(product, newAmount);
        }
        return createMassResponse(toRestock);
    }

    public List<ProductStockResponseDto> massDestock(MassRestockRequestDto massRestockRequestDto)
                                            throws ProductNotFoundException, NotEnoughStockException {
        HashMap<Product, Integer> toDestock = new HashMap<>();
        for(ProductRestockRequestDto destockInfo : massRestockRequestDto.getProducts()) {
            Product product = getProduct(destockInfo.getId()).orElseThrow(ProductNotFoundException::new);
            Integer newAmount = product.getAmount() - destockInfo.getChange();
            if (newAmount < 0) throw new NotEnoughStockException();
            toDestock.put(product, newAmount);
        }
        return createMassResponse(toDestock);
    }

    private List<ProductStockResponseDto> createMassResponse(HashMap<Product, Integer> toStock) {
        List<ProductStockResponseDto> massResponse = new ArrayList<>();
        for(Product product : toStock.keySet()) {
            product.setAmount(toStock.get(product));
            productRepository.save(product);
            massResponse.add(new ProductStockResponseDto(product.getId(), toStock.get(product)));
        }
        return massResponse;
    }
}
