package nl.utwente.soa.cmt.inventoryservice.exception;

import org.springframework.http.HttpStatus;

public class NotEnoughStockException extends ResponseException {

    public NotEnoughStockException() {
        super(HttpStatus.PRECONDITION_FAILED, "There is not enough product in stock to sell the desired amount.");
    }
}
