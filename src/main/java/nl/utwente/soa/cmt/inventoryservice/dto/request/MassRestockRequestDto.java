package nl.utwente.soa.cmt.inventoryservice.dto.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class MassRestockRequestDto {

    @NotNull
    @Size(min = 1)
    @Valid
    private List<ProductRestockRequestDto> products;

    public MassRestockRequestDto(List<ProductRestockRequestDto> productRestockRequestDtoList) {
        this.products = productRestockRequestDtoList;
    }

    public MassRestockRequestDto() {}
}
