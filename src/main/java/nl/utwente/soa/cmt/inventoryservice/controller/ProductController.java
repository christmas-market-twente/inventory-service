package nl.utwente.soa.cmt.inventoryservice.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import nl.utwente.soa.cmt.inventoryservice.dto.request.CreateProductRequestDto;
import nl.utwente.soa.cmt.inventoryservice.dto.request.UpdateProductRequestDto;
import nl.utwente.soa.cmt.inventoryservice.dto.response.ProductPresalesResponseDto;
import nl.utwente.soa.cmt.inventoryservice.dto.response.ProductResponseDto;
import nl.utwente.soa.cmt.inventoryservice.exception.ProductNotFoundException;
import nl.utwente.soa.cmt.inventoryservice.response.ErrorResponse;
import nl.utwente.soa.cmt.inventoryservice.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/products")
@Tag(name = "Product", description = "The Product API")
public class ProductController {

    @Autowired
    private ProductService productService;

    @Operation(summary = "Get a list of all products.", description = "Returns a list of all products.", tags = {"Product"})
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "Successful operation")})
    @GetMapping("")
    public List<ProductResponseDto> getAllProducts() {
        return productService.getAllProducts();
    }

    @Operation(summary = "Find a product by its ID.", description = "Returns a single product.", tags = {"Product"})
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "Successful operation"),
        @ApiResponse(responseCode = "404", description = "No product was found with the specified ID",
            content = @Content(schema = @Schema(implementation = ErrorResponse.class)))})
    @GetMapping("/{id}")
    public ProductResponseDto getProduct(@Parameter(description = "ID of the product to be obtained.", required = true)
                                             @PathVariable Long id) throws ProductNotFoundException {
        return productService.getProduct(id);
    }

    @Operation(summary = "Get presale information about a list of products", description = "Returns a list of products with information on their stock, price and seller.", tags = {"Product"})
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "Successful operation"),
        @ApiResponse(responseCode = "404", description = "One or multiple given IDs are not recognized as a product.",
            content = @Content(schema = @Schema(implementation = ErrorResponse.class)))})
    @GetMapping("/presale")
    public List<ProductPresalesResponseDto> getProductPresale(@Parameter(description = "List of product IDs to obtain information about.")
                                                @RequestParam Long[] ids) throws ProductNotFoundException {
        return productService.getProductPresale(ids);
    }

    @Operation(summary = "Create a new product.", description = "Creates a new product and returns it.", tags = {"Product"})
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "Successful operation"),
        @ApiResponse(responseCode = "400", description = "Invalid request body",
            content = @Content(schema = @Schema(implementation = ErrorResponse.class)))})
    @PostMapping("")
    public ProductResponseDto createProduct(@Parameter(description = "The product to be created.")
                                                @Valid @RequestBody CreateProductRequestDto createProductRequestDto) {
        return productService.createProduct(createProductRequestDto);
    }

    @Operation(summary = "Update a product.", description = "Updates a product and returns it.", tags = {"Product"})
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "Successful operation"),
        @ApiResponse(responseCode = "400", description = "Invalid request body",
            content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
        @ApiResponse(responseCode = "404", description = "No product was found with the specified ID",
            content = @Content(schema = @Schema(implementation = ErrorResponse.class)))})
    @PutMapping("/{id}")
    public ProductResponseDto updateProduct(@Parameter(description = "The ID of the product to be updated.", required = true)
                                                @PathVariable Long id,
                                            @Parameter(description = "The changed product information")
                                                @Valid @RequestBody UpdateProductRequestDto updateProductRequestDto) throws ProductNotFoundException {
        return productService.updateProduct(id, updateProductRequestDto);
    }

    @Operation(summary = "Delete a product.", description = "Deletes a product.", tags = {"Product"})
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "Successful operation"),
        @ApiResponse(responseCode = "404", description = "No product was found with the specified ID",
            content = @Content(schema = @Schema(implementation = ErrorResponse.class)))})
    @DeleteMapping("/{id}")
    public void deleteProduct(@Parameter(description = "The ID of the product to be deleted.", required = true)
                                  @PathVariable Long id) throws ProductNotFoundException {
        productService.deleteProduct(id);
    }
}
