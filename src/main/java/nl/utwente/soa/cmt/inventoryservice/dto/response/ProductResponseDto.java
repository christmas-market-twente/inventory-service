package nl.utwente.soa.cmt.inventoryservice.dto.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import nl.utwente.soa.cmt.inventoryservice.model.Product;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductResponseDto {

    private Long id;

    private String name;

    private String description;

    private Integer amount;

    private Integer price;

    private String vendor;

    private String imageUrl;

    public ProductResponseDto(Long id, String name, String description, Integer amount, Integer price, String vendor, String imageUrl) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.amount = amount;
        this.price = price;
        this.vendor = vendor;
        this.imageUrl = imageUrl;
    }

    public ProductResponseDto() {
    }

    public static ProductResponseDto from(Product product) {
        return new ProductResponseDto(
                product.getId(),
                product.getName(),
                product.getDescription(),
                product.getAmount(),
                product.getPrice(),
                product.getVendor(),
                product.getImageUrl()
        );
    }
}
