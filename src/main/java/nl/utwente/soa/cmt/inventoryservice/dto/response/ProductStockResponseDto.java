package nl.utwente.soa.cmt.inventoryservice.dto.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductStockResponseDto {

    private Long id;

    private Integer amount;

    public ProductStockResponseDto(Long id, Integer amount) {
        this.id = id;
        this.amount = amount;
    }
}
