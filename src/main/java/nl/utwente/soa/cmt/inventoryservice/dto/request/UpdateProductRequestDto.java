package nl.utwente.soa.cmt.inventoryservice.dto.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class UpdateProductRequestDto {

    @NotNull
    private String name;

    private String description;

    @NotNull
    @Min(0)
    private Integer price;

    private String imageUrl;
}
