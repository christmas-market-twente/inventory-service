package nl.utwente.soa.cmt.inventoryservice.exception;

import org.springframework.http.HttpStatus;

public class ProductNotFoundException extends ResponseException {

    public ProductNotFoundException() {
        super(HttpStatus.NOT_FOUND, "No product could be found with the given ID.");
    }


}
