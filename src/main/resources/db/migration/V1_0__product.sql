CREATE TABLE `products` (
    `id` BIGINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    `name` VARCHAR(100) NOT NULL,
    `description` TEXT,
    `amount` INT UNSIGNED NOT NULL,
    `price` INT UNSIGNED NOT NULL,
    `vendor` VARCHAR(36) NOT NULL,
    `image_url` VARCHAR(255)
);